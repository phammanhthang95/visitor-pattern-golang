package main

import "fmt"


// Interface Element chứa phương thức Accept.
type building interface {
	getType() string
	accept(visitor)
}

// Interface Visitor chứa phương thức Visit cho từng loại Element.
type visitor interface {
	visitForChurch(*church)
	visitForHotel(*hotel)
	visitForStadium(*stadium)
}

// church là một thành phần của cấu trúc dữ liệu.
type church struct {
	sideA int
	sideB int
}

// Phương thức Accept cho church.
func (c *church) accept(v visitor) {
	v.visitForChurch(c)
}

func (s *church) getType() string {
	return "Church"
}

// stadium là một thành phần của cấu trúc dữ liệu.
type stadium struct {
	radius int
}

// Phương thức Accept cho stadium.
func (s *stadium) accept(v visitor) {
	v.visitForStadium(s)
}

func (s *stadium) getType() string {
	return "Stadium"
}

// hotel là một thành phần của cấu trúc dữ liệu.
type hotel struct {
	a int
	b int
}

// Phương thức Accept cho hotel.
func (h *hotel) accept(v visitor) {
	v.visitForHotel(h)
}

func (h *hotel) getType() string {
	return "Hotel"
}

// Struct areaCalculator thực hiện hoạt động trên các thành phần của cấu trúc dữ liệu.
type areaCalculator struct {
	area int
}

// Phương thức visitForChurch thực hiện hoạt động trên struct church.
func (a *areaCalculator) visitForChurch(c *church) {
	fmt.Println("Calculator area for Church")
}

// Phương thức visitForStadium thực hiện hoạt động trên struct stadium.
func (a *areaCalculator) visitForStadium(c *stadium) {
	fmt.Println("Calculator area for Stadium")
}

// Phương thức visitForHotel thực hiện hoạt động trên struct hotel.
func (a *areaCalculator) visitForHotel(c *hotel) {
	fmt.Println("Calculator area for Hotel")
}

// Struct middleCoordinates thực hiện hoạt động trên các thành phần của cấu trúc dữ liệu.
type middleCoordinates struct {
	x int
	y int
}

func (a *middleCoordinates) visitForChurch(c *church) {
	fmt.Println("Calculator middle point coordinates for church")
}

func (a *middleCoordinates) visitForStadium(c *stadium) {
	fmt.Println("Calculator middle point coordinates for stadium")
}

func (a *middleCoordinates) visitForHotel(c *hotel) {
	fmt.Println("Calculator middle point coordinates for hotel")
}

func main() {
	church := &church{sideA: 2, sideB: 4}
	stadium := &stadium{radius: 3}
	hotel := &hotel{a: 2, b: 3}

	areaCalculator := &areaCalculator{}

	church.accept(areaCalculator)
	stadium.accept(areaCalculator)
	hotel.accept(areaCalculator)

	fmt.Println()

	middleCoordinates := &middleCoordinates{}

	church.accept(middleCoordinates)
	stadium.accept(middleCoordinates)
	hotel.accept(middleCoordinates)
}